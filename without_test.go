package set

import (
	"testing"
)

func TestWithout(t *testing.T) {
	t.Run("when the set contains the item", func(t *testing.T) {
		set := subject(u1, u2)

		result := set.Without(u1)

		t.Run("it returns a new set", func(t *testing.T) {
			if result == set {
				t.Errorf("expected %v, got %v", set, result)
			}
		})

		t.Run("the new set does not contain the item", func(t *testing.T) {
			if result.Contains(u1) {
				t.Errorf("expected %v to not contain %v", result, u1)
			}
		})

		t.Run("the new set retains all other items", func(t *testing.T) {
			if !result.Contains(u2) {
				t.Errorf("expected %v to contain %v", result, u2)
			}
		})
	})

	t.Run("when the set does not contain the item", func(t *testing.T) {
		set := subject(u1, u2)

		result := set.Without(u3)

		t.Run("it returns the original set", func(t *testing.T) {
			if result != set {
				t.Errorf("expected %v, got %v", set, result)
			}
		})
	})
}
