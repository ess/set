package set

import (
	"testing"
)

func TestWith(t *testing.T) {
	t.Run("when the set already contains the item", func(t *testing.T) {
		set := subject(u1, u2)

		result := set.With(u1)

		t.Run("it returns the original set", func(t *testing.T) {
			if result != set {
				t.Errorf("expected %v, got %v", set, result)
			}
		})
	})

	t.Run("when the set does not contain the item", func(t *testing.T) {
		set := subject(u1, u2)

		result := set.With(u3)

		t.Run("it returns a new set", func(t *testing.T) {
			if result == set {
				t.Errorf("expected %v, got %v", set, result)
			}
		})

		t.Run("the new set contains the item", func(t *testing.T) {
			if !result.Contains(u3) {
				t.Errorf("expected %v to contain %v", result, u3)
			}
		})

		t.Run("the new set contains the original items", func(t *testing.T) {
			if !result.Contains(u1) {
				t.Errorf("expected %v to contain %v", result, u1)
			}

			if !result.Contains(u2) {
				t.Errorf("expected %v to contain %v", result, u2)
			}
		})
	})
}
