package set

import (
	"testing"
)

func TestSet_IsSupersetOf(t *testing.T) {
	t.Run("when the set is empty", func(t *testing.T) {
		set := subject()

		t.Run("and the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.IsSupersetOf(other)

			t.Run("it returns true", func(t *testing.T) {
				if !result {
					t.Errorf("expected %v to be a superset of %v", set, other)
				}
			})
		})

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.IsSupersetOf(other)

			t.Run("it returns true", func(t *testing.T) {
				if !result {
					t.Errorf("expected %v to be a superset of %v", set, other)
				}
			})
		})

		t.Run("but the other set is not empty", func(t *testing.T) {
			other := subject(u1, u2)

			result := set.IsSupersetOf(other)

			t.Run("it returns false", func(t *testing.T) {
				if result {
					t.Errorf("expected %v to not be a superset of %v", set, other)
				}
			})
		})
	})

	t.Run("when the set is not empty", func(t *testing.T) {
		set := subject(u1, u2)

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.IsSupersetOf(other)

			t.Run("it returns true", func(t *testing.T) {
				if !result {
					t.Errorf("expected %v to be a superset of %v", set, other)
				}
			})
		})

		t.Run("but the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.IsSupersetOf(other)

			t.Run("it returns true", func(t *testing.T) {
				if !result {
					t.Errorf("expected %v to be a superset of %v", set, other)
				}
			})
		})

		t.Run("and the other set is not empty", func(t *testing.T) {
			t.Run("and the two sets are equal", func(t *testing.T) {
				other := subject(u1, u2)

				result := set.IsSupersetOf(other)

				t.Run("it returns true", func(t *testing.T) {
					if !result {
						t.Errorf("expected %v to be a superset of %v", set, other)
					}
				})
			})

			t.Run("and the set contains all elements of the other set", func(t *testing.T) {
				other := subject(u1)

				result := set.IsSupersetOf(other)

				t.Run("it returns true", func(t *testing.T) {
					if !result {
						t.Errorf("expected %v to be a superset of %v", set, other)
					}
				})
			})

			t.Run("and the other set is larger than the set", func(t *testing.T) {
				other := set.With(u4)

				result := set.IsSupersetOf(other)

				t.Run("it returns false", func(t *testing.T) {
					if result {
						t.Errorf("expected %v to not be a superset of %v", set, other)
					}
				})
			})
		})
	})
}
