package set

// With returns a new set with the given element added.
func (s *Set[T]) With(element T) *Set[T] {
	if s.Contains(element) {
		return s
	}

	// Create a new slice with the new element.
	elements := append(s.Elements(), element)

	// Create and return the new set.
	return New[T](elements, s.hash, s.equal)
}
