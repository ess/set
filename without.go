package set

// Without returns a new set with the given element removed.
func (s *Set[T]) Without(element T) *Set[T] {
	// Get the element's hash.
	hash := s.hash(element)

	// If the element is not present in the set, return the original set.
	_, ok := s.elements[hash]
	if !ok {
		return s
	}

	// Create a new map by copying the original map.
	newElements := make(map[uint64]T, len(s.elements)-1)
	for h, e := range s.elements {
		if h != hash {
			newElements[h] = e
		}
	}

	// Create and return the new set.
	return &Set[T]{
		elements: newElements,
		hash:     s.hash,
		equal:    s.equal,
	}
}
