package set

// Difference returns a new set with the given set's elements that are not in
// the set.
func (s *Set[T]) Difference(other *Set[T]) *Set[T] {
	if other == nil {
		// The other set is nil, so return the set.
		return s
	}

	if other.empty() {
		// The other set is empty, so return the set.
		return s
	}

	if s.empty() {
		// The set is empty, so return the set.
		return s
	}

	// Create a new map with the elements of s.
	newElements := make(map[uint64]T, s.Size())
	for h, e := range s.elements {
		if !other.Contains(e) {
			newElements[h] = e
		}
	}

	// Return the set.
	return &Set[T]{
		elements: newElements,
		hash:     s.hash,
		equal:    s.equal,
	}
}
