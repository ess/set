package set

import (
	"fmt"
	"testing"
)

func TestSet_String(t *testing.T) {
	t.Run("when the set is empty", func(t *testing.T) {
		set := subject()

		result := set.String()

		t.Run("it is a pair of empty curly braces", func(t *testing.T) {
			expected := "{}"
			if result != expected {
				t.Errorf("expected %v, got %v", expected, result)
			}
		})
	})

	t.Run("when the set has one element", func(t *testing.T) {
		set := subject(u1)

		result := set.String()

		t.Run("it is a pair of curly braces with the element inside", func(t *testing.T) {
			expected := fmt.Sprintf("{%v}", u1)
			if result != expected {
				t.Errorf("expected %v, got %v", expected, result)
			}
		})
	})

	t.Run("when the set has more than one element", func(t *testing.T) {
		set := subject(u1, u2)

		result := set.String()

		t.Run("it is a pair of curly braces with the elements inside", func(t *testing.T) {
			expected := fmt.Sprintf("{%v, %v}", u1, u2)
			if result != expected {
				t.Errorf("expected %v, got %v", expected, result)
			}
		})
	})
}
