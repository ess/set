package set

// IsProperSupersetOf returns true if the set is a proper superset of the given
// set.
func (s *Set[T]) IsProperSupersetOf(other *Set[T]) bool {
	if other == nil {
		other = New[T](make([]T, 0), s.hash, s.equal)
	}

	// The set is a proper superset of the other set if it is a superset of the
	// other set and the sets are not equal.
	return s.IsSupersetOf(other) && other.Size() < s.Size()
}
