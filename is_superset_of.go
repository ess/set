package set

// IsSupersetOf returns true if the set is a superset of the given set.
func (s *Set[T]) IsSupersetOf(other *Set[T]) bool {
	// Every set is a superset of the empty set.
	if other == nil {
		return true
	}

	// Every set is a superset of the empty set.
	if other.empty() {
		return true
	}

	// A set cannot be a superset of a set with more elements.
	if other.Size() > s.Size() {
		return false
	}

	// Both sets have elements, so check if every element in the other set is
	// also in the set.
	for _, element := range other.Elements() {
		if !s.Contains(element) {
			return false
		}
	}

	// The set is a superset of the other set.
	return true
}
