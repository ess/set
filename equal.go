package set

// Equal returns true if the given set is equal to the set.
func (s *Set[T]) Equal(other *Set[T]) bool {
	// Check the size.
	if s.Size() != other.Size() {
		return false
	}

	// The sets are the same size, so the sets are inequal if any element in the
	// set is not in the other set.
	for _, element := range s.elements {
		if !other.Contains(element) {
			return false
		}
	}

	// The sets are equal.
	return true
}
