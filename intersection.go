package set

// Intersection returns a set with the given set's elements that are also in
// the set.
func (s *Set[T]) Intersection(other *Set[T]) *Set[T] {
	if s.empty() {
		// The set is empty, and intersection with an empty set is empty, so return
		// the empty set.
		return s
	}

	// Create the elements slice.
	elements := make([]T, 0, s.Size())

	if other != nil {
		// The other set is empty, and intersection with an empty set is empty, so
		// return the empty set.
		if other.empty() {
			return other
		}

		// Add the elements to the slice.
		for _, element := range s.elements {
			if other.Contains(element) {
				elements = append(elements, element)
			}
		}
	}

	// Return the set.
	return New(elements, s.hash, s.equal)
}
