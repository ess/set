package set

// Elements returns the set's elements.
func (s *Set[T]) Elements() []T {
	// Create the elements slice.
	elements := make([]T, 0, len(s.elements))

	// Add the elements to the slice.
	for _, element := range s.elements {
		elements = append(elements, element)
	}

	// Return the elements.
	return elements
}
