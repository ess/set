package set

import (
	"testing"
)

func TestSet_Intersection(t *testing.T) {
	t.Run("when the set is empty", func(t *testing.T) {
		set := subject()

		t.Run("and the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.Intersection(other)

			t.Run("it returns the original set", func(t *testing.T) {
				if result != set {
					t.Errorf("expected %v, got %v", set, result)
				}
			})
		})

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.Intersection(other)

			t.Run("it returns the original set", func(t *testing.T) {
				if result != set {
					t.Errorf("expected %v, got %v", set, result)
				}
			})
		})

		t.Run("but the other set is not empty", func(t *testing.T) {
			other := subject(u1, u2)

			result := set.Intersection(other)

			t.Run("it returns an empty set", func(t *testing.T) {
				expected := subject()

				if !result.Equal(expected) {
					t.Errorf("expected %v, got %v", expected, result)
				}
			})
		})
	})

	t.Run("when the set is not empty", func(t *testing.T) {
		set := subject(u1, u2)

		t.Run("and the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.Intersection(other)

			t.Run("it returns an empty set", func(t *testing.T) {
				expected := subject()

				if !result.Equal(expected) {
					t.Errorf("expected %v, got %v", expected, result)
				}
			})
		})

		t.Run("but the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.Intersection(other)

			t.Run("it returns an empty set", func(t *testing.T) {
				expected := subject()

				if !result.Equal(expected) {
					t.Errorf("expected %v, got %v", expected, result)
				}
			})
		})

		t.Run("and the other set is not empty", func(t *testing.T) {
			other := subject(u2, u3, u4)

			result := set.Intersection(other)

			t.Run("it returns a new set with the intersection of the two sets", func(t *testing.T) {
				expected := subject(u2)

				if !result.Equal(expected) {
					t.Errorf("expected %v, got %v", expected, result)
				}
			})
		})
	})
}
