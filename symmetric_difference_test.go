package set

import (
	"testing"
)

func TestSet_SymmetricDifference(t *testing.T) {
	t.Run("when the set is empty", func(t *testing.T) {
		set := subject()

		t.Run("and the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.SymmetricDifference(other)

			t.Run("it returns the original set", func(t *testing.T) {
				if result != set {
					t.Errorf("expected %v, got %v", set, result)
				}
			})
		})

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.SymmetricDifference(other)

			t.Run("it returns the original set", func(t *testing.T) {
				if result != set {
					t.Errorf("expected %v, got %v", set, result)
				}
			})
		})

		t.Run("but the other set is not empty", func(t *testing.T) {
			other := subject(u1, u2)

			result := set.SymmetricDifference(other)

			t.Run("it returns the other set", func(t *testing.T) {
				if result != other {
					t.Errorf("expected %v, got %v", other, result)
				}
			})
		})
	})

	t.Run("when the set is not empty", func(t *testing.T) {
		set := subject(u1, u2)

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.SymmetricDifference(other)

			t.Run("it returns the original set", func(t *testing.T) {
				if result != set {
					t.Errorf("expected %v, got %v", set, result)
				}
			})
		})

		t.Run("but the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.SymmetricDifference(other)

			t.Run("it returns the original set", func(t *testing.T) {
				if result != set {
					t.Errorf("expected %v, got %v", set, result)
				}
			})
		})

		t.Run("and the other set is not empty", func(t *testing.T) {
			other := subject(u2, u3, u4)

			result := set.SymmetricDifference(other)

			t.Run("it returns a new set with the symmetric difference of the two sets", func(t *testing.T) {
				expected := subject(u1, u3, u4)

				if !result.Equal(expected) {
					t.Errorf("expected %v, got %v", expected, result)
				}
			})
		})
	})
}
