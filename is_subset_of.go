package set

// IsSubsetOf returns true if the set is a subset of the given set.
func (s *Set[T]) IsSubsetOf(other *Set[T]) bool {
	// The empty set is a subset of every set.
	if s.empty() {
		return true
	}

	// No set is a subset of the empty set.
	if other == nil {
		return false
	}

	// No set is a subset of the empty set.
	if other.empty() {
		return false
	}

	// Both sets have elements, so check if every element in the set is
	// also in the other set.
	for _, element := range s.elements {
		if !other.Contains(element) {
			return false
		}
	}

	// No discrepancies were found, so the set is a subset of the other set.
	return true
}
