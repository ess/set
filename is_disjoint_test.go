package set

import (
	"testing"
)

func TestSet_IsDisjoint(t *testing.T) {
	t.Run("when the set is empty", func(t *testing.T) {
		set := subject()

		t.Run("and the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.IsDisjoint(other)

			t.Run("it returns true", func(t *testing.T) {
				if !result {
					t.Errorf("expected %v to be disjoint from %v", set, other)
				}
			})
		})

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.IsDisjoint(other)

			t.Run("it returns true", func(t *testing.T) {
				if !result {
					t.Errorf("expected %v to be disjoint from %v", set, other)
				}
			})
		})

		t.Run("but the other set is not empty", func(t *testing.T) {
			other := subject(u1, u2)

			result := set.IsDisjoint(other)

			t.Run("it returns false", func(t *testing.T) {
				if result {
					t.Errorf("expected %v not to be disjoint from %v", set, other)
				}
			})
		})
	})

	t.Run("when the set is not empty", func(t *testing.T) {
		set := subject(u1, u2)

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.IsDisjoint(other)

			t.Run("it returns false", func(t *testing.T) {
				if result {
					t.Errorf("expected %v not to be disjoint from %v", set, other)
				}
			})
		})

		t.Run("but the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.IsDisjoint(other)

			t.Run("it returns false", func(t *testing.T) {
				if result {
					t.Errorf("expected %v not to be disjoint from %v", set, other)
				}
			})
		})

		t.Run("and the other set is not empty", func(t *testing.T) {
			t.Run("and the sets have no elements in common", func(t *testing.T) {
				other := subject(u3, u4)

				result := set.IsDisjoint(other)

				t.Run("it returns true", func(t *testing.T) {
					if !result {
						t.Errorf("expected %v to be disjoint from %v", set, other)
					}
				})
			})

			t.Run("but the sets have elements in common", func(t *testing.T) {
				other := subject(u2, u3)

				result := set.IsDisjoint(other)

				t.Run("it returns false", func(t *testing.T) {
					if result {
						t.Errorf("expected %v not to be disjoint from %v", set, other)
					}
				})
			})

		})
	})
}
