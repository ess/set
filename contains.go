package set

// Contains returns true if the given element is in the set.
func (s *Set[T]) Contains(element T) bool {
	// Get the element's hash.
	hash := s.hash(element)

	// Return whether or not the element is in the set.
	_, ok := s.elements[hash]
	return ok
}
