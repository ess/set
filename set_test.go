package set

type User struct {
	ID   int
	Name string
}

var hasher = func(u *User) uint64 {
	return uint64(u.ID)
}

var equality = func(u1, u2 *User) bool {
	return u1.ID == u2.ID
}

var u1 *User = &User{ID: 1, Name: "John"}
var u2 *User = &User{ID: 2, Name: "Jane"}
var u3 *User = &User{ID: 3, Name: "Joe"}
var u4 *User = &User{ID: 4, Name: "Jenny"}

var subject = func(users ...*User) *Set[*User] {
	return New[*User](users, hasher, equality)
}
