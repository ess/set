package set

// Size returns the number of elements in the set.
func (s *Set[T]) Size() int {
	return len(s.elements)
}
