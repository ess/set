package set

// SymmetricDifference returns a new set with the given set's elements that are
// not in the set and the set's elements that are not in the given set.
func (s *Set[T]) SymmetricDifference(other *Set[T]) *Set[T] {
	if other == nil {
		return s
	}

	return s.Difference(other).Union(other.Difference(s))
}
