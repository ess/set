package set

// IsProperSubsetOf returns true if the set is a proper subset of the given set.
func (s *Set[T]) IsProperSubsetOf(other *Set[T]) bool {
	if other == nil {
		other = New[T](make([]T, 0), s.hash, s.equal)
	}

	// The set is a proper subset of the other set if it is a subset of the other
	// set and the sets are not equal.
	return s.IsSubsetOf(other) && other.Size() > s.Size()
}
