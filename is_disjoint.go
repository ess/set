package set

// IsDisjoint returns true if the set is disjoint with the given set.
func (s *Set[T]) IsDisjoint(other *Set[T]) bool {
	if other == nil {
		other = New[T](make([]T, 0), s.hash, s.equal)
	}

	// Empty sets are disjoint.
	if s.empty() && other.empty() {
		return true
	}

	// A non-empty set is not disjoint with an empty set.
	if s.empty() || other.empty() {
		return false
	}

	// If the intersection of the two sets is empty, then they are disjoint.
	return s.Intersection(other).empty()
}
