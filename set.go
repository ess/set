// Package set provides a generic, immutable,  thread-safe set implementation for
// sets of arbitrary type T.
package set

import (
	"fmt"
	"sort"
	"strings"
)

// Type *Set provides a *Set set implementation. It is immutable and is safe for
// concurrent use.
type Set[T any] struct {
	// The set's elements.
	elements map[uint64]T

	// The set's hash function.
	hash func(T) uint64

	// The set's equality function.
	equal func(T, T) bool
}

// New returns a new Set with the given data (which is deduplicated), the
// given hash function, and the given equality function.
func New[T any](data []T, hash func(T) uint64, equal func(T, T) bool) *Set[T] {
	elementHashes := make(map[uint64]T)

	for _, element := range data {
		hash := hash(element)

		if _, ok := elementHashes[hash]; !ok {
			elementHashes[hash] = element
		}
	}

	// Create the set.
	s := &Set[T]{
		elements: elementHashes,
		hash:     hash,
		equal:    equal,
	}

	// Return the set.
	return s
}

// String returns a string representation of the set.
func (s *Set[T]) String() string {
	items := make([]string, 0, len(s.elements))

	for _, item := range s.elements {
		items = append(items, fmt.Sprintf("%v", item))
	}

	sort.Strings(items)
	return fmt.Sprintf("{%v}", strings.Join(items, ", "))
}

func (s *Set[T]) empty() bool {
	return s.Size() == 0
}
