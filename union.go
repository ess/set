package set

// Union returns a new set with the given set's elements added.
func (s *Set[T]) Union(other *Set[T]) *Set[T] {
	// Other is nil, which means that it's empty, so return the set.
	if other == nil {
		return s
	}

	// The other set is empty, so return the set.
	if other.empty() {
		return s
	}

	// The other set is not empty, but the set is, so return the other set.
	if s.empty() {
		return other
	}

	// Nobody's empty, so we have to actually do some work.
	return New(append(s.Elements(), other.Elements()...), s.hash, s.equal)
}
