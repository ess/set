package set

import (
	"testing"
)

func TestSet_IsProperSubsetOf(t *testing.T) {
	t.Run("when the set is empty", func(t *testing.T) {
		set := subject()

		t.Run("and the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.IsProperSubsetOf(other)

			t.Run("it returns false", func(t *testing.T) {
				if result {
					t.Errorf("expected %v to not be a proper subset of %v", set, other)
				}
			})
		})

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.IsProperSubsetOf(other)

			t.Run("it returns false", func(t *testing.T) {
				if result {
					t.Errorf("expected %v to not be a proper subset of %v", set, other)
				}
			})
		})

		t.Run("but the other set is not empty", func(t *testing.T) {
			other := subject(u1, u2)

			result := set.IsProperSubsetOf(other)

			t.Run("it returns true", func(t *testing.T) {
				if !result {
					t.Errorf("expected %v to be a proper subset of %v", set, other)
				}
			})
		})
	})

	t.Run("when the set is not empty", func(t *testing.T) {
		set := subject(u1, u2)

		t.Run("but the other set is nil", func(t *testing.T) {
			var other *Set[*User]

			result := set.IsProperSubsetOf(other)

			t.Run("it returns false", func(t *testing.T) {
				if result {
					t.Errorf("expected %v to not be a proper subset of %v", set, other)
				}
			})
		})

		t.Run("but the other set is empty", func(t *testing.T) {
			other := subject()

			result := set.IsProperSubsetOf(other)

			t.Run("it returns false", func(t *testing.T) {
				if result {
					t.Errorf("expected %v to not be a proper subset of %v", set, other)
				}
			})
		})

		t.Run("and the other set is not empty", func(t *testing.T) {
			other := subject(u1, u2, u3)

			result := set.IsProperSubsetOf(other)

			t.Run("and the set is a proper subset", func(t *testing.T) {
				if !result {
					t.Errorf("expected %v to be a proper subset of %v", set, other)
				}
			})

			t.Run("and the set is not a proper subset", func(t *testing.T) {
				other := subject(u1, u2)

				result := set.IsProperSubsetOf(other)

				t.Run("it returns false", func(t *testing.T) {
					if result {
						t.Errorf("expected %v to not be a proper subset of %v", set, other)
					}
				})
			})
		})
	})
}
